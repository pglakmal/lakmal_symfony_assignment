<?php

/* shoppingPage.html.twig */
class __TwigTemplate_91ee8e6f9c545aa379dc9c285b270262652ac4700ab3c05494bd5e8f99738ea0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shoppingPage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shoppingPage.html.twig"));

        // line 1
        echo "<html>
<head>
    <title>Lakmal Assignment</title>

    <!-- Compiled and minified CSS -->

    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css\">
    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">

    <!-- Compiled and minified JavaScript -->
    <script src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js\"></script>


    <style type=\"text/css\">

        .card {
            display: inline-block;
        }


    </style>
</head>
<body>

<nav>
    <div class=\"nav-wrapper\">
        <a href=\"#!\" class=\"brand-logo\">Book Store</a>

    </div>
</nav>

<div class=\"fixed-action-btn\">
    <a class=\"btn-floating btn-large red \" id=\"make-payment\">
        <i class=\"large material-icons tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Make a Payment\">payment</i>
    </a>
</div>

<div class=\"row\">
    <form action=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("payment");
        echo "\" method=\"post\" id=\"shopping-form\">
\t ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["books"]) ? $context["books"] : $this->getContext($context, "books")));
        foreach ($context['_seq'] as $context["category"] => $context["items"]) {
            // line 42
            echo "\t 
          <h3 class=\"header\">";
            // line 43
            echo twig_escape_filter($this->env, $context["category"], "html", null, true);
            echo "</h3>
        <div class=\"col s12 cards-container\">]
\t       
\t       ";
            // line 46
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["items"]);
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 47
                echo "                <div class=\"col s12 m4\">
                    <div class=\"card horizontal small\">
                        <div class=\"card-image\">
                            <img src=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "imageUrl", array()), "html", null, true);
                echo "\" width=\"200px\" height=\"auto\"
                                 style=\"width: auto; height: auto\">
                        </div>
                        <div class=\"card-stacked\">
                            <div class=\"card-content\">
                                <p class=\"card-title\"> ";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "</p>
                                <p class=\"card-title\"> \$ ";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "price", array()), "html", null, true);
                echo "</p>

                            </div>
                            <div class=\"card-action\">
                                <p>
                                    ";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "description", array()), "html", null, true);
                echo "
                                </p>
                                <p>
                                    <input name=\"shopping[item][";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "]\" type=\"number\" min=\"0\" max=\"10\"
                                           value=\"0\"  item-id=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "\"/>
                                    <input name=\"shopping[addToCart][";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "]\" type=\"checkbox\"
                                           id=\"item";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "\" item-name=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "\" item-desc=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "description", array()), "html", null, true);
                echo "\"
                                           item-url=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "imageUrl", array()), "html", null, true);
                echo "\" item-price=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "price", array()), "html", null, true);
                echo "\"
                                           item-id=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "\"
                                           item-category=\"";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "category", array()), "id", array()), "html", null, true);
                echo "\"
                                    />
                                    <label for=\"item";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
                echo "\">Add to Cart</label>
                                </p>

                            </div>

                        </div>
                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "        </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['category'], $context['items'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "    </form>
</div>

<a class=\"waves-effect waves-light btn modal-trigger\" href=\"#modal1\">Modal</a>

<!-- Modal Structure -->
<div id=\"selected-item-popup\" class=\"modal modal-fixed-footer\">
    <div class=\"modal-content\">

        <h4>Item Selected</h4>

        <ul class=\"collection\" id=\"item-list-collection\">

        </ul>

    </div>
    <div class=\"modal-footer\">
        <a href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\" id=\"pay-with-stripe\">Pay</a>
        <a href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\" id=\"\">Cancel</a>
    </div>
</div>


<script type=\"text/javascript\">

  
    \$(document).ready(function () {
        \$('.tooltipped').tooltip({delay: 50});
        var fullAmount = 0.00;
\t

        \$('#make-payment').click(function () {
            fullAmount = 0.00;

            var itemString= '<li class=\"collection-item avatar\"><img src=\"IMAGEURL\" alt=\"\" class=\"circle\"><span class=\"title\">ITEMNAME</span><p>ITEMDESCRIPTION</p><p><b>PRICECAL</b></p><a href=\"#!\" class=\"secondary-content\">SUBTOTAL</a> </li>';
            var totalString= '<li class=\"collection-item avatar\"><span class=\"title\"><b>Total</b></span><a href=\"#!\" class=\"secondary-content\">TOTAL</a> </li>';
           var discountString= '<li class=\"collection-item avatar\"><span class=\"title\"><b>Discount</b></span><a href=\"#!\" class=\"secondary-content\">DISCOUNT</a> </li>';
            var selectedItems =[];
            var itemQty = [];
            \$('#shopping-form *').filter(':input[type=number]').each(function(index, element){
                var itemId = \$(element).attr('item-id');
                var qty = \$(element).val();
                itemQty[itemId] =qty;
            });

            var list = '';
            var total = 0.00;
\t    var childrenBookCount =0;
            var fictionBookCount =0;

            var discount =0;
            \$('#shopping-form *').filter(':input[type=checkbox]').each(function(index, element){

                if(\$(element).is(':checked')){
                    
                    var temp = itemString;
                    temp = temp.replace('IMAGEURL',\$(element).attr('item-url') );
                    temp = temp.replace('ITEMNAME',\$(element).attr('item-name') );
                    temp = temp.replace('ITEMDESCRIPTION',\$(element).attr('item-desc') );
                    var price = parseFloat(itemQty[\$(element).attr('item-id')]) * parseFloat(\$(element).attr('item-price'));
                    temp = temp.replace('PRICECAL','\$ '+ \$(element).attr('item-price') + ' X '+itemQty[\$(element).attr('item-id')]);
                    temp = temp.replace('SUBTOTAL',price.toFixed(2));
                    list = list+ temp;
                    total= total+ price;
\t\t    if(\$(element).attr('item-category')=='2'){
\t\t\tfictionBookCount=fictionBookCount+parseInt(itemQty[\$(element).attr('item-id')]);
\t\t    }
\t\t    else{
                       childrenBookCount=childrenBookCount+parseInt(itemQty[\$(element).attr('item-id')]);

\t\t    }
                    
                    
                }

            });
            if(list==''){
                list = \"No Item Selected\";
            }else{
                if (childrenBookCount>=5){
\t\t\tdiscount=total*0.1;
\t\t}
                 if (fictionBookCount>=10 || childrenBookCount>=10){
\t\t\tdiscount=discount+(total*0.05);
\t\t}
                list=list+discountString.replace('DISCOUNT', parseFloat(discount).toFixed(2));
                total=total-discount;
                list = list+totalString.replace('TOTAL', parseFloat(total).toFixed(2));
            }
            fullAmount = parseFloat(total.toFixed(2));
            \$('#item-list-collection').html(list);
            \$('#selected-item-popup').modal();
            \$('#selected-item-popup').modal('open');

//           \$('#shopping-form').submit();
        });

        \$('#pay-with-stripe').click(function(e){

            e.preventDefault();
        });
    });

 
</script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shoppingPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 83,  168 => 81,  153 => 72,  148 => 70,  144 => 69,  138 => 68,  130 => 67,  126 => 66,  122 => 65,  118 => 64,  112 => 61,  104 => 56,  100 => 55,  92 => 50,  87 => 47,  83 => 46,  77 => 43,  74 => 42,  70 => 41,  66 => 40,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
<head>
    <title>Lakmal Assignment</title>

    <!-- Compiled and minified CSS -->

    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css\">
    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">

    <!-- Compiled and minified JavaScript -->
    <script src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js\"></script>


    <style type=\"text/css\">

        .card {
            display: inline-block;
        }


    </style>
</head>
<body>

<nav>
    <div class=\"nav-wrapper\">
        <a href=\"#!\" class=\"brand-logo\">Book Store</a>

    </div>
</nav>

<div class=\"fixed-action-btn\">
    <a class=\"btn-floating btn-large red \" id=\"make-payment\">
        <i class=\"large material-icons tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Make a Payment\">payment</i>
    </a>
</div>

<div class=\"row\">
    <form action=\"{{ path('payment') }}\" method=\"post\" id=\"shopping-form\">
\t {% for category, items in books %}
\t 
          <h3 class=\"header\">{{ category }}</h3>
        <div class=\"col s12 cards-container\">]
\t       
\t       {% for item in items %}
                <div class=\"col s12 m4\">
                    <div class=\"card horizontal small\">
                        <div class=\"card-image\">
                            <img src=\"{{ item.imageUrl }}\" width=\"200px\" height=\"auto\"
                                 style=\"width: auto; height: auto\">
                        </div>
                        <div class=\"card-stacked\">
                            <div class=\"card-content\">
                                <p class=\"card-title\"> {{ item.name }}</p>
                                <p class=\"card-title\"> \$ {{ item.price }}</p>

                            </div>
                            <div class=\"card-action\">
                                <p>
                                    {{ item.description }}
                                </p>
                                <p>
                                    <input name=\"shopping[item][{{ item.id }}]\" type=\"number\" min=\"0\" max=\"10\"
                                           value=\"0\"  item-id=\"{{ item.id }}\"/>
                                    <input name=\"shopping[addToCart][{{ item.id }}]\" type=\"checkbox\"
                                           id=\"item{{ item.id }}\" item-name=\"{{ item.name }}\" item-desc=\"{{ item.description }}\"
                                           item-url=\"{{ item.imageUrl }}\" item-price=\"{{ item.price }}\"
                                           item-id=\"{{ item.id }}\"
                                           item-category=\"{{ item.category.id }}\"
                                    />
                                    <label for=\"item{{ item.id }}\">Add to Cart</label>
                                </p>

                            </div>

                        </div>
                    </div>
                </div>
            {% endfor %}
        </div>
            {% endfor %}
    </form>
</div>

<a class=\"waves-effect waves-light btn modal-trigger\" href=\"#modal1\">Modal</a>

<!-- Modal Structure -->
<div id=\"selected-item-popup\" class=\"modal modal-fixed-footer\">
    <div class=\"modal-content\">

        <h4>Item Selected</h4>

        <ul class=\"collection\" id=\"item-list-collection\">

        </ul>

    </div>
    <div class=\"modal-footer\">
        <a href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\" id=\"pay-with-stripe\">Pay</a>
        <a href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\" id=\"\">Cancel</a>
    </div>
</div>


<script type=\"text/javascript\">

  
    \$(document).ready(function () {
        \$('.tooltipped').tooltip({delay: 50});
        var fullAmount = 0.00;
\t

        \$('#make-payment').click(function () {
            fullAmount = 0.00;

            var itemString= '<li class=\"collection-item avatar\"><img src=\"IMAGEURL\" alt=\"\" class=\"circle\"><span class=\"title\">ITEMNAME</span><p>ITEMDESCRIPTION</p><p><b>PRICECAL</b></p><a href=\"#!\" class=\"secondary-content\">SUBTOTAL</a> </li>';
            var totalString= '<li class=\"collection-item avatar\"><span class=\"title\"><b>Total</b></span><a href=\"#!\" class=\"secondary-content\">TOTAL</a> </li>';
           var discountString= '<li class=\"collection-item avatar\"><span class=\"title\"><b>Discount</b></span><a href=\"#!\" class=\"secondary-content\">DISCOUNT</a> </li>';
            var selectedItems =[];
            var itemQty = [];
            \$('#shopping-form *').filter(':input[type=number]').each(function(index, element){
                var itemId = \$(element).attr('item-id');
                var qty = \$(element).val();
                itemQty[itemId] =qty;
            });

            var list = '';
            var total = 0.00;
\t    var childrenBookCount =0;
            var fictionBookCount =0;

            var discount =0;
            \$('#shopping-form *').filter(':input[type=checkbox]').each(function(index, element){

                if(\$(element).is(':checked')){
                    
                    var temp = itemString;
                    temp = temp.replace('IMAGEURL',\$(element).attr('item-url') );
                    temp = temp.replace('ITEMNAME',\$(element).attr('item-name') );
                    temp = temp.replace('ITEMDESCRIPTION',\$(element).attr('item-desc') );
                    var price = parseFloat(itemQty[\$(element).attr('item-id')]) * parseFloat(\$(element).attr('item-price'));
                    temp = temp.replace('PRICECAL','\$ '+ \$(element).attr('item-price') + ' X '+itemQty[\$(element).attr('item-id')]);
                    temp = temp.replace('SUBTOTAL',price.toFixed(2));
                    list = list+ temp;
                    total= total+ price;
\t\t    if(\$(element).attr('item-category')=='2'){
\t\t\tfictionBookCount=fictionBookCount+parseInt(itemQty[\$(element).attr('item-id')]);
\t\t    }
\t\t    else{
                       childrenBookCount=childrenBookCount+parseInt(itemQty[\$(element).attr('item-id')]);

\t\t    }
                    
                    
                }

            });
            if(list==''){
                list = \"No Item Selected\";
            }else{
                if (childrenBookCount>=5){
\t\t\tdiscount=total*0.1;
\t\t}
                 if (fictionBookCount>=10 || childrenBookCount>=10){
\t\t\tdiscount=discount+(total*0.05);
\t\t}
                list=list+discountString.replace('DISCOUNT', parseFloat(discount).toFixed(2));
                total=total-discount;
                list = list+totalString.replace('TOTAL', parseFloat(total).toFixed(2));
            }
            fullAmount = parseFloat(total.toFixed(2));
            \$('#item-list-collection').html(list);
            \$('#selected-item-popup').modal();
            \$('#selected-item-popup').modal('open');

//           \$('#shopping-form').submit();
        });

        \$('#pay-with-stripe').click(function(e){

            e.preventDefault();
        });
    });

 
</script>
</body>
</html>
", "shoppingPage.html.twig", "/var/www/html/lakmal_assignment/app/Resources/views/shoppingPage.html.twig");
    }
}

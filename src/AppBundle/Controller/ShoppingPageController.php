<?php

namespace AppBundle\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Book;

class ShoppingPageController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $number = mt_rand(0, 100);

        $books = $this->getDoctrine()
            ->getRepository(Book::class)
            ->findAll();
	$data=array();
	foreach ($books as $book)
	{
		$data[$book->getCategory()->getName()][]=$book;
	}
        return $this->render('shoppingPage.html.twig', array(
            'books' => $data,
  
        ));
    }
}

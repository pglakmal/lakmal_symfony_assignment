<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!

        $items = array(
          array(
              'name'=>'Brave New World',
              'price'=>400,
              'desc'=>'Sample description for Brave New World',
              'url'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcSxG5karYbjK9reDzCP-xTdKmhw0CFS58AXDAXVb8B2Vd2jz_YH',
	      'category'=>'1'
          ),
            array(
              'name'=>'The Lord of the Rings',
              'price'=>500,
                'desc'=>'Sample description for The Lord of the Rings',
                'url'=>'http://t0.gstatic.com/images?q=tbn:ANd9GcTTGk79W3HtuDN9ht6ZHpDJnHVgVk2KT-SE0J2tCmD9DoiVgLeb',
	      'category'=>'1'
          ),  array(
              'name'=>'Ulysses',
              'price'=>550,
                'desc'=>'Sample description for Ulysses',
                'url'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcS7qd4aX5-q0fN6GBygZbAtDur_Adk7i_5IiFBozBrqdu9mz-m5',
	      'category'=>'1'
          )
        ,  array(
              'name'=>'Jane Eyre',
              'price'=>10,
                'desc'=>'Sample description for Jane Eyre',
                'url'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcT1UPpu8CvnqpeHrLtarGaoQoNjS6s8HRhuKlo4EOxqyqcJX-KR',
	      'category'=>'1'
          ),  array(
              'name'=>'Slaughterhouse-Five',
              'price'=>650,
                'desc'=>'Sample description for Slaughterhouse-Five',
                'url'=>'http://t0.gstatic.com/images?q=tbn:ANd9GcTmf1_g-9bHA1eVD8t2clePQHyQXd90Mnp_eF-29xSdjCzwMqtF',
	      'category'=>'1'
          ),
            array(
                'name'=>'Dune',
                'price'=>700,
                'desc'=>'Sample description for Kindle Paperwhite',
                'url'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcSQb5b5RhsDzW7FXm6nMv9evsWqWbV8bFNtJPrscCLvvkjX7p8j',
	      'category'=>'1'
            ),
            array(
                'name'=>'War and Peace',
                'price'=>500,
                'desc'=>'Sample description for War and Peace',
                'url'=>'http://t0.gstatic.com/images?q=tbn:ANd9GcRVveXk-guLHUlXWWe8b4ZMSzg6U0sOA-MSk4U67NmpvKa2k8Qp',
	      'category'=>'1'
            ),  array(
                'name'=>'Allice In Wonderland',
                'price'=>300,
                'desc'=>'Sample description for Allice In Wonderland',
                'url'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcS-12EYp7EAi9mB7doTvtOzrtjL91li2OentfE4kdktSd-ThSGB',
	      'category'=>'2'
            )
        ,  array(
                'name'=>'Anna Karenina',
                'price'=>10,
                'desc'=>'Sample description for Anna Karenina',
                'url'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcTwcY5vuFPdzgcHxUmUPHsOtFAylKk95Ch4W40_EAydjUeTuoN0',
	      'category'=>'1'
            ),  array(
                'name'=>'Harry Porter',
                'price'=>350,
                'desc'=>'Sample description for Harry Porter',
                'url'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcQIOhZ8K3T2EWfEvOV67l2n0JDIk817wf-S2-DR-VyxbL4HSo0F',
	      'category'=>'2'
            ),

        );
      
	
	$kidsCategory = new Category();
	$kidsCategory->setName('Children');
      
 	$manager->persist($kidsCategory);

	$fiction = new Category();
	$fiction->setName('Fiction');
        
 	$manager->persist($fiction);


        for ($i = 0; $i < count($items); $i++) {
            $item = new Book();
            $item->setName($items[$i]['name']);
            $item->setPrice($items[$i]['price']);
            $item->setDescription($items[$i]['desc']);
            $item->setImageUrl($items[$i]['url']);
            if ($items[$i]['category']=='1'){
		  $item->setCategory($fiction);
	    }else{
		  $item->setCategory($kidsCategory);
	    }
	  
            $manager->persist($item);
        }

        $manager->flush();
    }
}
